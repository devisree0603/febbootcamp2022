package week2.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class FirstAssessment_Devisree {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		ChromeOptions options=new ChromeOptions();
		options.addArguments("--disable-notifications");
		RemoteWebDriver driver = new ChromeDriver(options);
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);
		driver.findElement(By.className("slds-icon-waffle")).click();
		driver.findElement(By.xpath("//button[contains(text(),'View All')]")).click();
		Thread.sleep(2000);
		
		//CreateDashboard
		
		WebElement dashBoard = driver.findElement(By.xpath("//span/p[text()='Dashboards']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", dashBoard);
		Thread.sleep(5000);
		dashBoard.click();
		driver.findElement(By.xpath("//div[text()='New Dashboard']")).click();
		
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@id='dashboardNameInput']")).sendKeys("DevisreeJ_workout");
		driver.findElement(By.xpath("//input[@id='dashboardDescriptionInput']")).sendKeys("Automation");
		driver.findElement(By.xpath("//button[text()='Create']")).click();
		
		driver.switchTo().defaultContent();
		Thread.sleep(3000);
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()='Save']")).click();
		driver.switchTo().defaultContent();
		@SuppressWarnings("deprecation")
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText' and text()='Dashboard saved']")));
				//visibilityOfElementLocated(By.xpath("//span[text()='Dashboard saved']")));
		String dashBoardSaved = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText' and text()='Dashboard saved']")).getText();
		if(dashBoardSaved.equals("Dashboard saved"))
		{
			System.out.println("Dashboard Saved successfully");
		}
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()='Done']")).click();
		
		String DashBoardName = driver.findElement(By.xpath("//span[text()='DevisreeJ_workout']")).getText();
		if(DashBoardName.equals("DevisreeJ_workout"))
		{
			System.out.println("Dashboard Created successfully");
		}
		Thread.sleep(3000);
		
		// SubscribeDashboard
		
		driver.findElement(By.xpath("//button[text()='Subscribe']")).click();
		Thread.sleep(3000);
		
		driver.switchTo().defaultContent();
		
		driver.findElement(By.xpath("//span[text()='Daily']")).click();
		
		Select dropDown=new Select(driver.findElement(By.xpath("//select")));
		dropDown.selectByVisibleText("10:00 AM");
		
		driver.findElement(By.xpath("//span[text()='Save']")).click();
		
		
		
		WebDriverWait wait1= new WebDriverWait(driver,20);
		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='You started a dashboard subscription.']"))) ;
		String toastMessage1 = driver.findElement(By.xpath("//span[text()='You started a dashboard subscription.']")).getText();
		if(toastMessage1.contains("dashboard subscription"))
			{
				System.out.println("Dashboard Subscription is set");
			}
	
		
		driver.findElement(By.xpath("//span[text()='Close DevisreeJ_workout']/..")).click();
		//deleteDashboard
		
		driver.findElement(By.xpath("//span[text()='Dashboards']")).click();
		driver.findElement(By.xpath("(//a[@title='Private Dashboards' and text()='Private Dashboards'])[1]")).click();
		driver.findElement(By.xpath("//input[@placeholder='Search private dashboards...']")).sendKeys("DevisreeJ_workout");
		//JavascriptExecutor js=(JavascriptExecutor)driver;
		//js.executeScript("window.scrollBy(500,0)");
		driver.findElement(By.xpath("//div[@class='slds-scrollable_y']")).click();
		driver.findElement(By.xpath("(//tr/td)[6]")).click();
		Actions act= new Actions(driver);
		act.moveToElement(driver.findElement(By.xpath("//span[text()='Delete']"))).click().perform();
		driver.findElement(By.xpath("//button[@title='Delete']")).click();
		@SuppressWarnings("deprecation")
		WebDriverWait wait2=new WebDriverWait(driver,30);
		wait2.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//span[contains(text(),'Dashboard')])[5]"))));
		String delMess=driver.findElement(By.xpath("(//span[contains(text(),'Dashboard')])[5]")).getText();
		System.out.println(delMess);
		if(delMess.contains("deleted"))
		{
			System.out.println("Dashboard deleted");
		}
		
		String result = driver.findElement(By.xpath("//span[@class=\"emptyMessageTitle\"]")).getText();
		
		if (result.contains("No results found")){
			System.out.println("Deletion successful");
		}

		
	}

}
