package week1.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CreateTaskNoMandatory {

	public static void main(String[] args) throws InterruptedException {

		WebDriverManager.chromedriver().setup();
		ChromeOptions options=new ChromeOptions();
		options.addArguments("--disable-notifications");
		RemoteWebDriver driver=new ChromeDriver(options);
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		
		driver.manage().timeouts().implicitlyWait(9000, TimeUnit.MILLISECONDS);
		driver.findElement(By.className("slds-icon-waffle")).click();
		driver.findElement(By.xpath("//button[contains(text(),'View All')]")).click();
		driver.findElement(By.xpath("//p[text()='Sales']")).click();
		
		driver.findElement(By.xpath("//a[@title='Tasks']/following-sibling::one-app-nav-bar-item-dropdown")).click();
		
		WebElement Tasks = driver.findElement(By.xpath("//span[text()='New Task']"));
		Actions action1=new Actions(driver);
		action1.moveToElement(Tasks).click().perform();
		Thread.sleep(3000);
		Actions action=new Actions(driver);
		driver.findElement(By.xpath("//input[@title='Search Contacts']")).click();
		action.moveToElement(driver.findElement(By.xpath("(//a[@role='option']/div)[2]"))).click().perform();
		driver.findElement(By.xpath("//textarea[@role='textbox']")).sendKeys("SALES FORCE Automation Using Selenium");
		driver.findElement(By.xpath("//span[text()='Save']")).click();
		String errorMess = driver.findElement(By.xpath("//span[text()='Review the errors on this page.']")).getText();
		System.out.println(errorMess);
		String errorMessage = driver.findElement(By.xpath("//li[contains(text(),'These required fields must be completed')]")).getText();
		if(errorMessage.contains("Subject"))
		{
			System.out.println("Error message for Mandatory field subject is displayed");
		}
		driver.close();
	}

}
