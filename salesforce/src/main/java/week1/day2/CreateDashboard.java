package week1.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CreateDashboard {

	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		ChromeOptions options=new ChromeOptions();
		options.addArguments("--disable-notifications");
		RemoteWebDriver driver = new ChromeDriver(options);
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("makaia@testleaf.com");
		driver.findElementById("password").sendKeys("BootcampSel$123");
		driver.findElementById("Login").click();
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);
		driver.findElementByClassName("slds-icon-waffle").click();
		driver.findElementByXPath("//button[contains(text(),'View All')]").click();
		Thread.sleep(2000);
		//JavascriptExecutor js=(JavascriptExecutor)driver;
		//js.executeScript("window.scrollBy(0,1000)");
		WebElement dashBoard = driver.findElementByXPath("//span/p[text()='Dashboards']");
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", dashBoard);
		Thread.sleep(5000);
		dashBoard.click();
		driver.findElementByXPath("//div[text()='New Dashboard']").click();
		driver.switchTo().frame(driver.findElementByXPath("//iframe[@title='dashboard']"));
		driver.findElementByXPath("//input[@id='dashboardNameInput']").sendKeys("Salesforce Automation by Devisree");
		driver.findElementByXPath("//input[@id='dashboardDescriptionInput']").sendKeys("Automation");
		driver.findElementByXPath("//button[text()='Create']").click();
		driver.switchTo().defaultContent();
		Thread.sleep(3000);
		driver.switchTo().frame(driver.findElementByXPath("//iframe[@title='dashboard']"));
		Thread.sleep(3000);
		driver.findElementByXPath("//button[text()='Save']").click();
		driver.switchTo().defaultContent();
		@SuppressWarnings("deprecation")
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText' and text()='Dashboard saved']")));
				//visibilityOfElementLocated(By.xpath("//span[text()='Dashboard saved']")));
		String dashBoardSaved = driver.findElementByXPath("//span[@class='toastMessage slds-text-heading--small forceActionsText' and text()='Dashboard saved']").getText();
		if(dashBoardSaved.equals("Dashboard saved"))
		{
			System.out.println("Dashboard created successfully");
		}
		driver.close();
	}

}
