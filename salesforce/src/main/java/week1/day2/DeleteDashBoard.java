package week1.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DeleteDashBoard {

	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		ChromeOptions options=new ChromeOptions();
		options.addArguments("--disable-notifications");
		RemoteWebDriver driver = new ChromeDriver(options);
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("makaia@testleaf.com");
		driver.findElementById("password").sendKeys("BootcampSel$123");
		driver.findElementById("Login").click();
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);
		driver.findElementByClassName("slds-icon-waffle").click();
		driver.findElementByXPath("//button[contains(text(),'View All')]").click();
		Thread.sleep(2000);
		//JavascriptExecutor js=(JavascriptExecutor)driver;
		//js.executeScript("window.scrollBy(0,1000)");
		WebElement dashBoard = driver.findElementByXPath("//span/p[text()='Dashboards']");
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", dashBoard);
		dashBoard.click();
		Thread.sleep(5000);
		driver.findElementByXPath("//input[@placeholder='Search recent dashboards...']").sendKeys("Salesforce Automation by Devisree");
		Thread.sleep(2000);
		((JavascriptExecutor)driver).executeScript("window.scrollBy(500,0)");
		driver.findElementByXPath("(//tr/td)[6]").click();
		Actions act= new Actions(driver);
		act.moveToElement(driver.findElementByXPath("//span[text()='Delete']")).click().perform();
		driver.findElementByXPath("//button[@title='Delete']").click();
		@SuppressWarnings("deprecation")
		WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOf(driver.findElementByXPath("(//span[contains(text(),'Dashboard')])[5]")));
		String delMess=driver.findElementByXPath("(//span[contains(text(),'Dashboard')])[5]").getText();
		System.out.println(delMess);
		if(delMess.contains("deleted"))
		{
			System.out.println("Dashboard deleted");
		}
		driver.close();
	}

}
