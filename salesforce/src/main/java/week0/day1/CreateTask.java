package week0.day1;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.tools.ant.util.PermissionUtils.FileType;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import io.github.bonigarcia.wdm.WebDriverManager;
import junit.framework.Assert;

public class CreateTask extends BaseClass {

	@SuppressWarnings("deprecation")
	@Test	
	public void createTask() throws IOException {
	
		driver.findElementByXPath("//a[@title='Tasks']/following-sibling::one-app-nav-bar-item-dropdown").click();
		Actions action=new Actions(driver);
		Actions action1=new Actions(driver);
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);
		WebElement newTask = driver.findElementByXPath("//span[text()='New Task']");
		action.moveToElement(newTask).click().perform();
		driver.findElementByXPath("(//div[@class='slds-combobox__form-element slds-input-has-icon slds-input-has-icon_right']/input)[2]").sendKeys("Bootcamp");
		driver.findElementByXPath("//input[@title='Search Contacts']").click();
		action.moveToElement(driver.findElementByXPath("(//a[@role='option']/div)[2]/div[@title='Natrayan']")).click().perform();
		driver.findElementByXPath("//a[@role='button' and text()='Not Started']").click();
		action1.moveToElement(driver.findElementByXPath("//a[@title='Waiting on someone else']")).click().perform();
		driver.findElementByXPath("//span[text()='Save']").click();
		
		WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@title='Bootcamp']/div[text()='Bootcamp']")));
		String bootcampText = driver.findElementByXPath("//a[@title='Bootcamp']/div[text()='Bootcamp']").getText();
		System.out.println(bootcampText);
		if(bootcampText.equals("Bootcamp"))
		{
			System.out.println("Task created");
		}
		/*
		 SoftAssert assert1=new SoftAssert();
		assert1.assertEquals(actual, expected);
	*/
		File source=driver.getScreenshotAs(OutputType.FILE);
		File target=new File("./snap/CreateTask.png");
		FileUtils.copyFile(source, target);
		
		
	}
	

}
