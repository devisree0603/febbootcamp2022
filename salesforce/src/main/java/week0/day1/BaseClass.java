package week0.day1;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseClass {
	public ChromeDriver driver;
	@Parameters({"url","uname","pwd"})
	@BeforeMethod
	public void preCondition(String url,String uname,String pwd) {
		WebDriverManager.chromedriver().setup();
		ChromeOptions options= new ChromeOptions();
		options.addArguments("--disable-notifications");
		driver = new ChromeDriver(options);
		driver.get(url);
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys(uname);
		driver.findElementById("password").sendKeys(pwd);
		driver.findElementById("Login").click();
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);
		driver.findElementByClassName("slds-icon-waffle").click();
		driver.findElementByXPath("//button[contains(text(),'View All')]").click();
		driver.findElementByXPath("//p[text()='Sales']").click();
	
	}
	
	@AfterMethod
	public void postCondition()
	{
		driver.close();
	}
	
}
