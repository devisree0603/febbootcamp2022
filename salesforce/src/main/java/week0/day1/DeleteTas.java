package week0.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DeleteTas {

	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		ChromeOptions options= new ChromeOptions();
		options.addArguments("--disable-notifications");
		RemoteWebDriver driver = new ChromeDriver(options);
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("makaia@testleaf.com");
		driver.findElementById("password").sendKeys("BootcampSel$123");
		driver.findElementById("Login").click();
		
		driver.manage().timeouts().implicitlyWait(9000, TimeUnit.MILLISECONDS);
		driver.findElementByClassName("slds-icon-waffle").click();
		driver.findElementByXPath("//button[contains(text(),'View All')]").click();
		driver.findElementByXPath("//p[text()='Sales']").click();
		
		WebElement Tasks = driver.findElementByXPath("//span[text()='Tasks']");
		
		Actions action1=new Actions(driver);
		action1.moveToElement(Tasks).click().perform();
		Thread.sleep(3000);
		
		Actions action3=new Actions(driver);
		action3.moveToElement(driver.findElementByXPath("//button[@title='Display as Split View']/lightning-primitive-icon")).click().perform();
		
		Actions action2=new Actions(driver);
		action2.moveToElement(driver.findElementByXPath("(//a[@role='menuitemcheckbox'])[1]")).click().perform();
		
		driver.findElementByXPath("(//tr/th/span/a[@title='Bootcamp']/ancestor::th/parent::tr/td)[7]").click();
		
		Actions action4=new Actions(driver);
		action4.moveToElement(driver.findElementByXPath("//a[@title='Delete']/div[text()='Delete']")).click().perform();
		
		driver.findElementByXPath("//span[text()='Delete']").click();
		
		@SuppressWarnings("deprecation")
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']")));
		
		//action3.moveToElement(driver.findElementByXPath("//a[@title='Delete']/div[@title='Delete']")).click().perform();
		
		String deleteTask = driver.findElementByXPath("//span[@class='toastMessage slds-text-heading--small forceActionsText']").getText();
		System.out.println(deleteTask);
		if(deleteTask.contains("deleted"))
		{
			System.out.println("Task is successfully deleted");
		}
		driver.close();
	}

}
