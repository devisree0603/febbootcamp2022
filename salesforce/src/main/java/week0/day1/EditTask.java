package week0.day1;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.asserts.SoftAssert;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EditTask {

	public static void main(String[] args) throws IOException {
		
WebDriverManager.chromedriver().setup();
		
		ChromeOptions options= new ChromeOptions();
		options.addArguments("--disable-notifications");
		RemoteWebDriver driver = new ChromeDriver(options);
		driver.get("https://login.salesforce.com/");
		driver.manage().timeouts().implicitlyWait(9000, TimeUnit.MILLISECONDS);
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("makaia@testleaf.com");
		driver.findElementById("password").sendKeys("BootcampSel$123");
		driver.findElementById("Login").click();
		driver.findElementByClassName("slds-icon-waffle").click();
		driver.findElementByXPath("//button[contains(text(),'View All')]").click();
		driver.findElementByXPath("//p[text()='Sales']").click();
		Actions action=new Actions(driver);
		Actions action1=new Actions(driver);
		Actions action2=new Actions(driver);
		Actions action3=new Actions(driver);
		WebElement Tasks = driver.findElementByXPath("//span[text()='Tasks']");
		action.moveToElement(Tasks).click().perform();
		driver.findElementByXPath("//button[@title='Display as Split View']/lightning-primitive-icon").click();
		WebElement Table = driver.findElementByXPath("(//a[@role='menuitemcheckbox'])[1]");
		action1.moveToElement(Table).click().perform();
		driver.findElementByXPath("(//tr/th/span/a[@title='Bootcamp']/ancestor::th/parent::tr/td)[7]").click();
		WebElement Edit = driver.findElementByXPath("//a[@title='Edit']/div[@class='forceActionLink' and @title='Edit']");
		action2.moveToElement(Edit).click().perform();
		String Subject = driver.findElementByXPath("//input[@data-value='Bootcamp']").getAttribute("data-value");
		System.out.println(Subject);
		if(Subject.equals("Bootcamp"))
		{
			System.out.println("Subject Verified");
		}
		//SoftAssert assert1=new SoftAssert();
		//assert1.assertEquals(Subject,"Bootcamp");
		
		DateFormat dateFormat=new SimpleDateFormat("MM/dd/yyyy");
		Date date=new Date();
		String d1=dateFormat.format(date);
		driver.findElementByXPath("//label[text()='Due Date']/following-sibling::div/input").clear();
		driver.findElementByXPath("//label[text()='Due Date']/following-sibling::div/input").sendKeys(d1);
		
		driver.findElementByXPath("(//span[text()='Priority']/following::div/div/div/div/a)[8]").click();
		action3.moveToElement(driver.findElementByXPath("(//li[@class='uiMenuItem uiRadioMenuItem'])[4]/a")).click().perform();
		driver.findElementByXPath("(//button/span[text()='Save'])[2]").click();
		
		File source=driver.getScreenshotAs(OutputType.FILE);
		File target=new File("./snap/Edit.png");
		FileUtils.copyFile(source, target);
		driver.quit();

	}

}
